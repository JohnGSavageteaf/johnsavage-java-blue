package com.techelevator.dao;

import com.techelevator.model.Park;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

public class JdbcParkDao implements ParkDao {

    private final JdbcTemplate jdbcTemplate;
//                      constructor
    public JdbcParkDao(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    } //            instantiates and implements this

    @Override
    public Park getPark(long parkId) {

    // Declare a park
    Park myPark = new Park();
    // write my sql statement
    String parkSql = "SELECT park_id, park_name, date_established, area, has_camping" +
    "From park" +
    "WHERE park_id = ?;";
    // run my sql statement
    SqlRowSet results = jdbcTemplate.queryForRowSet(parkSql, parkId);
    // MAp my park to a Park object
    if(results.next()) {
    myPark.setParkId(results.getLong("parkId"));
    myPark.setParkName(results.getString("park_name"));
    myPark.setDateEstablished(results.getDate("date_established").toLocalDate());
    myPark.setArea(results.getDouble("area")) ;


    }

    return myPark;
    }
    @Override
    public List<Park> getParksByState(String stateAbbreviation) {
        return new ArrayList<Park>();
    }

    @Override
    public Park createPark(Park park) {
        return new Park();
    }

    @Override
    public void updatePark(Park park) {

    }

    @Override
    public void deletePark(long parkId) {

    }

    @Override
    public void addParkToState(long parkId, String stateAbbreviation) {


    }

    @Override
    public void removeParkFromState(long parkId, String stateAbbreviation) {

    }

    private Park mapRowToPark(SqlRowSet results) {
        Park thePark;
        thePark = new Park();
        thePark.setParkId(results.getLong("parkId"));
        thePark.setDateEstablished(results.getDate("date_established").toLocalDate());
        thePark.setParkName(results.getString("park_name"));
        thePark.setArea(results.getDouble("area"));

        return new Park();
    }
}
