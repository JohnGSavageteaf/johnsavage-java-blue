package com.techelevator;

import java.util.Scanner;

class DiscountCalculator {

    /**
     * The main method is the start and end of our program
     */
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Welcome to the Discount Calculator");

        // Prompt the user for a discount amount
        // The answer needs to be saved as a double
        System.out.print("Enter the discount amount (w/out percentage): ");
        // declare string to accept input
        String userDiscount = scanner.nextLine();
       // declare double to get double out of user input
        double discountPercent = Double.parseDouble(userDiscount) / 100;


        // Prompt the user for a series of prices
        System.out.print("Please provide a series of prices (space separated): ");
        String userPricesToDiscount = scanner.nextLine();
        // split prices input to them to array of doubles

            //1split string -> array of strings
        String[] arr$trPrice;
        arr$trPrice = userPricesToDiscount.split(" ");

            //2 use array to build array of doubles

        //double[] prices = new double[arr$trPrice.length];

        // 2v1 - iterate over string array, parse, calculate, print

        for (int i = 0; i < arr$trPrice.length; i++); {

            double originalPrice = Double.parseDouble(arr$trPrice[i]);

           System.out.println("Original Price: $ " +
                   originalPrice +
                   "Discounted Price: $ "
                   + (originalPrice * (1 - dicountPercent)));


        }





    }

}