package com.techelevator;

public class StringShennanigans {
    public static void main (String[] args) {

        String name1 = "Steve";
        String name2 = "Steve";

        System.out.println("Steve == (Steve) " + name1 == name2);
        System.out.println("Steve.Equals (Steve) " + name1.equals(name2) );
    }
}
